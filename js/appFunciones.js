/* Funciones Evidencia 4 */

/* 1. Diseña una función que reciba como argumento un arreglo de valores 
enteros de 20 posiciones , regrese el valor promedio de los elementos del arreglo. */

function promedio(arreglo) {
    let sum = 0;
    for(let i=0; i<arreglo.length; i++){
        sum = sum + arreglo[i];
    }

    let res = sum/arreglo.length;
    return res
}

let numeros = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200];

console.log('El promedio del arreglo es ' + promedio(numeros));


/* 2. Diseñe una función que reciba como argumento un arreglo de 20 valores 
numéricos enteros, y me regrese la cantidad de valores pares que existe en el 
arreglo */

function parImpar(arreglo2) {
    let pares = 0;
    for(let i=0; i<arreglo2.length; i++){
        
        let numero = arreglo2[i];
        if(numero % 2 === 0){
            pares = pares + 1;
        }
        
    }
    return pares;
}

function imparPar(arreglo2) {
    let impares = 0;
    for(let i=0; i<arreglo2.length; i++){
        
        let numero = arreglo2[i];
        if(numero % 2 != 0){
            impares = impares + 1;
        }
        
    }
    return impares;
}

let numeros2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

console.log('La cantidad de numeros par es ' + parImpar(numeros2));


/* 3. Diseñe una función que reciba como argumento un arreglo de 20 valores 
numéricos enteros, ordene los valores del arreglo de mayor a menor. */


function mayMen(arreglo3){
    let band = false;

    while (!band) {
        band = true;
        for(let i=0; i<arreglo3.length; i++){
            if (arreglo3[i] < arreglo3[i + 1]){
                aux = arreglo3[i + 1];
                arreglo3[i + 1] = arreglo3[i];
                arreglo3[i] = aux;
                band = false
            }
        }
    }
    return arreglo3;
}

function ordenAsc(arreglo3){
    let band = false;

    while (!band) {
        band = true;
        for(let i=0; i<arreglo3.length; i++){
            if (arreglo3[i] > arreglo3[i + 1]){
                aux = arreglo3[i + 1];
                arreglo3[i + 1] = arreglo3[i];
                arreglo3[i] = aux;
                band = false
            }
        }
    }
    return arreglo3;
}

let numeros3 = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000];

console.log('Los numeros del arreglo de mayor a menor son ' + mayMen(numeros3));

// función para mostrar los resultados de las otras funciones
function mostrar(){
    let arregloF1 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200];
    let arregloF2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    let arregloF3 = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000];

    a1=document.getElementById('arrF1');
    a1.innerHTML=arregloF1;
    a2=document.getElementById('arrF2');
    a2.innerHTML=arregloF2;
    a3=document.getElementById('arrF3');
    a3.innerHTML=arregloF3;

    f1=document.getElementById('func1');
    f2=document.getElementById('func2');
    f3=document.getElementById('func3');
    let pdio=promedio(arregloF1);
    let numPar=parImpar(arregloF2);
    let mayorAMenor=mayMen(arregloF3);

    f1.innerHTML=pdio;
    f2.innerHTML=numPar;
    f3.innerHTML=mayorAMenor;
}


function llenar(){
    var limite = document.getElementById('limite').value;
    var Listanumeros = document.getElementById('numeros');
    var numOrd = new Array();

    while (Listanumeros.options.length > 0) {
        Listanumeros.remove(0);
    }


    for(let con = 0; con<limite; con++){
        let aleatorio = Math.floor((Math.random() * (50 - 1 + 1)) + 1);
        numOrd[con] = aleatorio;
    }

    for(let con = 0; con<limite; con++){
        ordenAsc(numOrd);
        Listanumeros.options[con] = new Option(numOrd[con], 'valor: ' + con);
    }

    
    // Contar la cantidad de numeros par e impar
    numsPar = parImpar(numOrd);
    numsImpar = imparPar(numOrd);

    // Mostrar el porcentaje de pares e impares
    porPar = (numsPar * 100 / limite).toFixed(2);
    porImpar = (numsImpar * 100 / limite).toFixed(2);

    pP = document.getElementById('porPares');
    pP.innerHTML=porPar+'%'
    pI = document.getElementById('porImpares');
    pI.innerHTML=porImpar+'%'

    // Determinar si el porcentaje es simetrico
    eS = document.getElementById('esSimetrico');
    if(porImpar - porPar > 25 || porPar - porImpar > 25){
        eS.innerHTML = 'No es simetrico'
    }else{
        eS.innerHTML = 'Es simetrico'
    }
    


    // hacer commit 'Generacion de numeros aleatorios'
    // hacer commit 'Validacion de caja de texto (limit) REQUERIDO Y NUMERICO'
    // hacer commit con la listaNumeros ORDENADOS ASCENDENTE
}

function validarLimit(){
    limit = document.querySelector('#limite').value;
    limitVacio = document.querySelector('#errorLimite');

    // condicion ? si : no
    limit == "" || limit == 0 ? limitVacio.style.visibility = 'visible' : limitVacio.style.visibility = 'hidden';

    if (limit == 0){
        alert("Capture los datos requeridos");
        eS.innerHTML = ''
        pP.innerHTML = ''
        pI.innerHTML = ''
    }
}

// contar los numeros pares e impares para saber su porcentaje
// la diferencia no sea mayor a 25%